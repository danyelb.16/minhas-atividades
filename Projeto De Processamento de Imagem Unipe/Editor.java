import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Arrays;

public class Editor {
	
	
	public static BufferedImage bandaR (BufferedImage img) {
        int largura = img.getWidth();
        int altura = img.getHeight();
        BufferedImage imgSaida = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
        for (int linha = 0; linha < altura; linha++) {
            for (int coluna = 0; coluna < largura; coluna++) {
            	
                int rgb = img.getRGB(coluna, linha);
                Color cor = new Color(rgb);
                int red = cor.getRed();
                
                Color redApenas = new Color(red, 0, 0);
                imgSaida.setRGB(coluna, linha, redApenas.getRGB());
            }
        }
        return imgSaida;
    }
		public BufferedImage bandaB (BufferedImage img) {
        int largura = img.getWidth();
        int altura = img.getHeight();
        BufferedImage imgSaida = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
        for (int linha = 0; linha < altura; linha++) {
            for (int coluna = 0; coluna < largura; coluna++) {
                int rgb = img.getRGB(coluna, linha);
                Color cor = new Color(rgb);
                int green = cor.getGreen();
                Color blueApenas = new Color(0, green, 0);
                imgSaida.setRGB(coluna, linha, blueApenas.getRGB());
            }
        }
        return imgSaida;
    }

    public static BufferedImage bandaG (BufferedImage img) {
        int largura = img.getWidth();
        int altura = img.getHeight();
        BufferedImage imgSaida = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
        for (int linha = 0; linha < altura; linha++) {
            for (int coluna = 0; coluna < largura; coluna++) {
                int rgb = img.getRGB(coluna, linha);
                Color cor = new Color(rgb);
                int blue = cor.getBlue();
                Color greenApenas = new Color(0, 0, blue);
                imgSaida.setRGB(coluna, linha, greenApenas.getRGB());
            }
        }
        return imgSaida;
        
    }
    
    
    public static BufferedImage UpTonalidade (BufferedImage img, int valor,int op) {
        int largura = img.getWidth();
        int altura = img.getHeight();
        int opcao = 0;
        BufferedImage imgSaida = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);      
        for (int linha = 0; linha < altura; linha++) {
            for (int coluna = 0; coluna < largura; coluna++) {
                int rgb = img.getRGB(coluna, linha);
                Color cor = new Color(rgb);
                int red = cor.getRed();
                int green = cor.getGreen();
                int blue = cor.getBlue();
                Color novacor = null;
                
                if(op==1) {
                	red = red + valor;
                	if(red>255) red =255;
                	if(red<0) red =0;
                	
                	novacor = new Color(red,green,blue);
                	imgSaida.setRGB(coluna, linha, novacor.getRGB());
                }
                if(op==2) {
                	green= green + valor;
                     if (green > 255) green = 255;
                     if (green < 0) green = 0;
                     Color tonalidade = new Color(red, green, blue);
                     imgSaida.setRGB(coluna, linha, tonalidade.getRGB());
                }
                if(op==3) {
                	 blue= blue + valor;
                     if (blue > 255) blue = 255;
                     if (blue < 0) blue = 0;
                     Color tonalidade = new Color(red, green, blue);
                     imgSaida.setRGB(coluna, linha, tonalidade.getRGB());
                }    
            }
        }
        return imgSaida;
    }
    
    
    public static BufferedImage EscalaCinzaBanda (BufferedImage img,int op) {
        int largura = img.getWidth();
        int altura = img.getHeight();
        int opcao = 0;
        BufferedImage imgSaida = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
        for (int linha = 0; linha < altura; linha++) {
            for (int coluna = 0; coluna < largura; coluna++) {
            	
                int rgb = img.getRGB(coluna, linha);
                Color cor = new Color(rgb);
                int red = cor.getRed();
                int green = cor.getGreen();
                int blue = cor.getBlue();
                Color novacor = null;
                
                if(op==1) {
                	novacor = new Color(red,red,red);
                	imgSaida.setRGB(coluna, linha, novacor.getRGB());
                }
                if(op==2) {      	
                     Color tonalidade = new Color(green, green, green);
                     imgSaida.setRGB(coluna, linha, tonalidade.getRGB());
                }
                if(op==3) {	
                     Color tonalidade = new Color(blue, blue, blue);
                     imgSaida.setRGB(coluna, linha, tonalidade.getRGB());
                }  
            }
        }
        return imgSaida;
    }
    
    
    public static BufferedImage EscaladeCinzaBandar (BufferedImage img) {
        int largura = img.getWidth();
        int altura = img.getHeight();
        BufferedImage imgSaida = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
        for (int linha = 0; linha < altura; linha++) {
            for (int coluna = 0; coluna < largura; coluna++) {
                int rgb = img.getRGB(coluna, linha);
                Color cor = new Color(rgb);
                int red = cor.getRed();
                int green = cor.getGreen();
                int blue = cor.getBlue();
                int bandaR = (red + green + blue)/3;
                Color escaladecinzaBandr = new Color(bandaR, bandaR, bandaR);
                imgSaida.setRGB(coluna, linha, escaladecinzaBandr.getRGB());
            }
        }
        return imgSaida;
    }
    
    
    public static BufferedImage BandaNegativa (BufferedImage img) {
        int largura = img.getWidth();
        int altura = img.getHeight();
        BufferedImage imgSaida = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
        for (int linha = 0; linha < altura; linha++) {
            for (int coluna = 0; coluna < largura; coluna++) {	
                int rgb = img.getRGB(coluna, linha);
                Color cor = new Color(rgb);
                int red = cor.getRed();
                int green = cor.getGreen();
                int blue = cor.getBlue();
                
                Color escalanegativa = new Color(255-red, 255-green, 255-blue);
                imgSaida.setRGB(coluna, linha, escalanegativa.getRGB());
            }
        }
        return imgSaida;
    }
    
    
    public double[][][] rgb2yiq (BufferedImage img){
        int largura = img.getWidth();
        int altura = img.getHeight();

        double[][][] imgYIQ = new double[altura][largura][3];

        for(int linha=0; linha < altura; linha++){
            for(int coluna = 0; coluna < largura; coluna++){
                int rgb =  img.getRGB(coluna,linha);
                Color cor = new Color(rgb);
                int red =  cor.getRed();
                int green = cor.getGreen();
                int blue = cor.getBlue();
                double y = (double) (0.299*red + 0.587*green + 0.114*blue); //Y
                double i= (double) (0.596*red - 0.274*green - 0.322*blue); //I
                double q = (double) (0.211*red - 0.523*green + 0.312*blue); //Q
                imgYIQ[linha][coluna][0] = y;
                imgYIQ[linha][coluna][1] = i;
                imgYIQ[linha][coluna][2] = q;

                
            }
        }

        return imgYIQ;
    }

    public BufferedImage yiq2RGB (double[][][] imgYIQ){
        int altura = imgYIQ.length;
        int largura = imgYIQ[0].length;
        BufferedImage imgSaida = new BufferedImage(largura,altura,BufferedImage.TYPE_INT_RGB);

        for(int linha=0; linha < altura; linha++){
            for(int coluna = 0; coluna < largura; coluna++){

                int red = (int) (imgYIQ[linha][coluna][0] + 0.956 * imgYIQ[linha][coluna][1] + 0.621 * imgYIQ[linha][coluna][2]);
                int green = (int) (imgYIQ[linha][coluna][0] - 0.272* imgYIQ[linha][coluna][1] - 0.647* imgYIQ[linha][coluna][2]);
                int blue = (int) (imgYIQ[linha][coluna][0] - 1.106 * imgYIQ[linha][coluna][1] + 1.703 * imgYIQ[linha][coluna][2]);

                Color novaCor = new Color(red,green,blue);
                imgSaida.setRGB(coluna,linha,novaCor.getRGB());
            }
        }
        return imgSaida;
    }
    
    public static BufferedImage Limiarizacao (BufferedImage img,int limear) {
        int largura = img.getWidth();
        int altura = img.getHeight();
        BufferedImage imgSaida = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);      
        for (int linha = 0; linha < altura; linha++) {
            for (int coluna = 0; coluna < largura; coluna++) {
                int rgb = img.getRGB(coluna, linha);
                Color cor = new Color(rgb);
                int red = cor.getRed();
                int green = cor.getGreen();
                int blue = cor.getBlue();
                int bandaR = (red + green + blue)/3;
                red = green = blue = bandaR;
                
                if(red<=limear) red =0;
                if(red>limear) red =255;
                
                if(green<=limear) green =0;
                if(green>limear) green =255;
                
                if(blue<=limear) blue =0;
                if(blue>limear) blue =255;
                
                Color brilho = new Color(red, green, blue);
                imgSaida.setRGB(coluna, linha, brilho.getRGB());
                }    
            }
        return imgSaida;
    }
    
    public static BufferedImage BrilhoAddRgb (BufferedImage img,int valor) {
        int largura = img.getWidth();
        int altura = img.getHeight();
        BufferedImage imgSaida = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);      
        for (int linha = 0; linha < altura; linha++) {
            for (int coluna = 0; coluna < largura; coluna++) {
                int rgb = img.getRGB(coluna, linha);
                Color cor = new Color(rgb);
                int red = cor.getRed();
                int green = cor.getGreen();
                int blue = cor.getBlue();
                
                red = red + valor;
                if(red>255) red =255;
                if(red<0) red =0;
                
                green= green + valor;
                if (green > 255) green = 255;
                if (green < 0) green = 0;
                     
                blue= blue + valor;
                if (blue > 255) blue = 255;
                if (blue < 0) blue = 0;
                Color brilho = new Color(red, green, blue);
                imgSaida.setRGB(coluna, linha, brilho.getRGB());
                }    
            }
        return imgSaida;
    }
    
    public static BufferedImage BrilhoMttRgb (BufferedImage img,double valor) {
        int largura = img.getWidth();
        int altura = img.getHeight();
        BufferedImage imgSaida = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);      
        for (int linha = 0; linha < altura; linha++) {
            for (int coluna = 0; coluna < largura; coluna++) {
                int rgb = img.getRGB(coluna, linha);
                Color cor = new Color(rgb);
                double red = cor.getRed();
                double green = cor.getGreen();
                double blue = cor.getBlue();
                
                red = red * valor;
                if(red>255) red =255;
                if(red<0) red =0;
                int red1 = (int) red;
                
                green= green * valor;
                if (green > 255) green = 255;
                if (green < 0) green = 0;
                int green1 = (int) green;
                blue= blue * valor;
                if (blue > 255) blue = 255;
                if (blue < 0) blue = 0;
                int blue1 = (int) blue;
                
                Color brilhoMtt = new Color(red1, green1, blue1);
                imgSaida.setRGB(coluna, linha, brilhoMtt.getRGB());
                }    
            }
        return imgSaida;
    }
    
    
    
    public static BufferedImage BrilhoAddY (BufferedImage img,int valor) {
        int largura = img.getWidth();
        int altura = img.getHeight();
        double[][][] imgYIQ = new double[altura][largura][3];
        BufferedImage imgSaida = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
        for (int linha = 0; linha < altura; linha++) {
            for (int coluna = 0; coluna < largura; coluna++) {	
                int rgb = img.getRGB(coluna, linha);
                Color cor = new Color(rgb);
                int red = cor.getRed();
                int green = cor.getGreen();
                int blue = cor.getBlue();
                
                double y = (double) (0.299*red + 0.587*green + 0.114*blue); //Y
                double i= (double) (0.596*red - 0.274*green - 0.322*blue); //I
                double q = (double) (0.211*red - 0.523*green + 0.312*blue); //Q
                imgYIQ[linha][coluna][0] = valor+y;
                imgYIQ[linha][coluna][1] = i;
                imgYIQ[linha][coluna][2] = q;
                
            	
                red = (int) (imgYIQ[linha][coluna][0] + 0.956 * imgYIQ[linha][coluna][1] + 0.621 * imgYIQ[linha][coluna][2]);
                green = (int) (imgYIQ[linha][coluna][0] - 0.272* imgYIQ[linha][coluna][1] - 0.647* imgYIQ[linha][coluna][2]);
                blue = (int) (imgYIQ[linha][coluna][0] - 1.106 * imgYIQ[linha][coluna][1] + 1.703 * imgYIQ[linha][coluna][2]);
                
                if(red>255) red =255;
            	if(red<0) red =0;
                
            	if(green>255) green =255;
            	if(green<0) green =0;
            	
            	if(blue>255) blue =255;
            	if(blue<0) blue =0;
                
                
                Color brilhoaddY = new Color(red, green, blue);
                imgSaida.setRGB(coluna, linha, brilhoaddY.getRGB());
            }
        }
        return imgSaida;
    }
    
    public static BufferedImage BrilhoMttY (BufferedImage img,double valor) {
        int largura = img.getWidth();
        int altura = img.getHeight();
        double[][][] imgYIQ = new double[altura][largura][3];
        BufferedImage imgSaida = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
        for (int linha = 0; linha < altura; linha++) {
            for (int coluna = 0; coluna < largura; coluna++) {	
                int rgb = img.getRGB(coluna, linha);
                Color cor = new Color(rgb);
                int red = cor.getRed();
                int green = cor.getGreen();
                int blue = cor.getBlue();
                
                double y = (double) (0.299*red + 0.587*green + 0.114*blue); //Y
                double i= (double) (0.596*red - 0.274*green - 0.322*blue); //I
                double q = (double) (0.211*red - 0.523*green + 0.312*blue); //Q
                imgYIQ[linha][coluna][0] = valor*y;
                imgYIQ[linha][coluna][1] = i;
                imgYIQ[linha][coluna][2] = q;
                
            	
                red = (int) (imgYIQ[linha][coluna][0] + 0.956 * imgYIQ[linha][coluna][1] + 0.621 * imgYIQ[linha][coluna][2]);
                green = (int) (imgYIQ[linha][coluna][0] - 0.272* imgYIQ[linha][coluna][1] - 0.647* imgYIQ[linha][coluna][2]);
                blue = (int) (imgYIQ[linha][coluna][0] - 1.106 * imgYIQ[linha][coluna][1] + 1.703 * imgYIQ[linha][coluna][2]);
                
                if(red>255) red =255;
            	if(red<0) red =0;
                
            	if(green>255) green =255;
            	if(green<0) green =0;
            	
            	if(blue>255) blue =255;
            	if(blue<0) blue =0;
                
                
                Color brilhomttY = new Color(red, green, blue);
                imgSaida.setRGB(coluna, linha, brilhomttY.getRGB());
            }
        }
        return imgSaida;
    }
   
    public static BufferedImage BandaNegativaY (BufferedImage img) {
        int largura = img.getWidth();
        int altura = img.getHeight();
        double[][][] imgYIQ = new double[altura][largura][3];
        BufferedImage imgSaida = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);
        for (int linha = 0; linha < altura; linha++) {
            for (int coluna = 0; coluna < largura; coluna++) {	
                int rgb = img.getRGB(coluna, linha);
                Color cor = new Color(rgb);
                int red = cor.getRed();
                int green = cor.getGreen();
                int blue = cor.getBlue();
                
                double y = (double) (0.299*red + 0.587*green + 0.114*blue); //Y
                double i= (double) (0.596*red - 0.274*green - 0.322*blue); //I
                double q = (double) (0.211*red - 0.523*green + 0.312*blue); //Q
                imgYIQ[linha][coluna][0] = 255-y;
                imgYIQ[linha][coluna][1] = i;
                imgYIQ[linha][coluna][2] = q;
                
            	
                red = (int) (imgYIQ[linha][coluna][0] + 0.956 * imgYIQ[linha][coluna][1] + 0.621 * imgYIQ[linha][coluna][2]);
                green = (int) (imgYIQ[linha][coluna][0] - 0.272* imgYIQ[linha][coluna][1] - 0.647* imgYIQ[linha][coluna][2]);
                blue = (int) (imgYIQ[linha][coluna][0] - 1.106 * imgYIQ[linha][coluna][1] + 1.703 * imgYIQ[linha][coluna][2]);
                
                if(red>255) red =255;
            	if(red<0) red =0;
                
            	if(green>255) green =255;
            	if(green<0) green =0;
            	
            	if(blue>255) blue =255;
            	if(blue<0) blue =0;
                
                
                Color escalanegativaY = new Color(red, green, blue);
                imgSaida.setRGB(coluna, linha, escalanegativaY.getRGB());
            }
        }
        return imgSaida;
    }
    
    
    public static BufferedImage media3x3 (BufferedImage imgOriginal) {
        BufferedImage imgFiltrada = imgOriginal;
        int linha = imgFiltrada.getWidth();
        int coluna = imgFiltrada.getHeight();
        for (int i = 1; i < linha -1; i++) {
            for (int j= 1; j < coluna -1; j++) {

                Color cor1 = new Color(imgOriginal.getRGB(i-1, j-1));
                Color cor2 = new Color(imgOriginal.getRGB(i-1, j));
                Color cor3 = new Color(imgOriginal.getRGB(i-1, j+1));
                Color cor4 = new Color(imgOriginal.getRGB(i, j-1));
                Color cor5 = new Color(imgOriginal.getRGB(i, j));
                Color cor6 = new Color(imgOriginal.getRGB(i, j+1));
                Color cor7 = new Color(imgOriginal.getRGB(i+1, j-1));
                Color cor8 = new Color(imgOriginal.getRGB(i+1, j));
                Color cor9 = new Color(imgOriginal.getRGB(i+1, j+1));

                int r1 = cor1.getRed();
                int r2 = cor2.getRed();
                int r3 = cor3.getRed();
                int r4 = cor4.getRed();
                int r5 = cor5.getRed();
                int r6 = cor6.getRed();
                int r7 = cor7.getRed();
                int r8 = cor8.getRed();
                int r9 = cor9.getRed();

                int media = (r1 + r2 + r3 + r4 + r5 + r6 + r7 + r8 + r9)/ 9;

                Color cor = new Color(media, media, media);

                imgFiltrada.setRGB(i, j, cor.getRGB());
            }
        }
        return imgFiltrada;
    }

    public static BufferedImage mediana3x3 (BufferedImage imgOriginal) {
        BufferedImage imgFiltrada = imgOriginal;
        int linha = imgFiltrada.getWidth();
        int coluna = imgFiltrada.getHeight();

        for (int i = 1; i < linha -1; i++) {
            for (int j= 1; j < coluna -1; j++) {

                Color cor1 = new Color(imgOriginal.getRGB(i-1, j-1));
                Color cor2 = new Color(imgOriginal.getRGB(i-1, j));
                Color cor3 = new Color(imgOriginal.getRGB(i-1, j+1));
                Color cor4 = new Color(imgOriginal.getRGB(i, j-1));
                Color cor5 = new Color(imgOriginal.getRGB(i, j));
                Color cor6 = new Color(imgOriginal.getRGB(i, j+1));
                Color cor7 = new Color(imgOriginal.getRGB(i+1, j-1));
                Color cor8 = new Color(imgOriginal.getRGB(i+1, j));
                Color cor9 = new Color(imgOriginal.getRGB(i+1, j+1));

                int r1 = cor1.getRed();
                int r2 = cor2.getRed();
                int r3 = cor3.getRed();
                int r4 = cor4.getRed();
                int r5 = cor5.getRed();
                int r6 = cor6.getRed();
                int r7 = cor7.getRed();
                int r8 = cor8.getRed();
                int r9 = cor9.getRed();

                int[] vetor = {r1, r2, r3, r4, r5, r6, r7, r8, r9};
                Arrays.sort(vetor);
                int media = vetor[(vetor.length-1)/2];

                Color cor = new Color(media, media, media);

                imgFiltrada.setRGB(i, j, cor.getRGB());
            }
        }
        return imgFiltrada;
    }

    public static BufferedImage mediaTamVizinhanca (BufferedImage imgOriginal, int tamVizinhanca) { //MEDIA ESCOLHENDO TAMANHO DA VIZINHANÇA
        BufferedImage imgFiltrada = imgOriginal;
        int largura = imgFiltrada.getWidth(); //largura vai entrar no if comparando com a linha
        int altura = imgFiltrada.getHeight(); //altura vai entrar no if comparando com a coluna

        int valorRef = tamVizinhanca/2;

        for (int linha = valorRef; linha < altura - valorRef; linha++) {
            for (int coluna = valorRef; coluna < largura - valorRef; coluna++) {
                int soma = 0;

                for (int i = - valorRef; i <= valorRef; i++) { // linha vizinhanÃ§a
                    for (int j = - valorRef; j <= valorRef; j++) {
                        Color cor = new Color(imgOriginal.getRGB(coluna + j, linha + i));
                        int red = cor.getRed();
                        soma += red;
                    }

                }

                int media = soma/(tamVizinhanca*tamVizinhanca);
                Color novaCor = new Color(media, media, media);
                imgFiltrada.setRGB(coluna, linha, novaCor.getRGB());
            }
        }
        return imgFiltrada;
    }

    public static BufferedImage medianaTamVizinhanca (BufferedImage imgOriginal, int tamVizinhanca) {
        BufferedImage imgFiltrada = imgOriginal;
        int largura = imgFiltrada.getWidth(); //largura vai entrar no if comparando com a linha
        int altura = imgFiltrada.getHeight(); //altura vai entrar no if comparando com a coluna

        int valorRef = tamVizinhanca/2;

        for (int linha = valorRef; linha < altura - valorRef; linha++) {
            for (int coluna = valorRef; coluna < largura - valorRef; coluna++) {
                int[] valores = new int [tamVizinhanca*tamVizinhanca];
                int posicao = 0;

                for (int i = - valorRef; i <= valorRef; i++) { // linha vizinhanÃ§a
                    for (int j = - valorRef; j <= valorRef; j++) {
                        Color cor = new Color(imgOriginal.getRGB(coluna + j, linha + i));
                        int red = cor.getRed();
                        valores[posicao] += red;
                        posicao++;
                    }

                }

                Arrays.sort(valores);
                int mediana = valores[(valores.length-1)/2];

                Color novaCor = new Color(mediana, mediana, mediana);
                imgFiltrada.setRGB(coluna, linha, novaCor.getRGB());
            }
        }
        return imgFiltrada;
    }


    public static BufferedImage convolucao (BufferedImage imgOriginal, double[] kernel) {

        int largura = imgOriginal.getWidth();
        int altura = imgOriginal.getHeight();
        BufferedImage imgFiltrada = new BufferedImage(largura, altura, BufferedImage.TYPE_INT_RGB);


        int tamVizinhanca = (int)Math.sqrt(kernel.length);
        int valorRef = tamVizinhanca/2;

        if (tamVizinhanca == 3) {

            for (int linha = valorRef; linha < altura - valorRef; linha++) {
                for (int coluna = valorRef; coluna < largura - valorRef; coluna++) {
                    double soma = 0;
                    int posicao = 0;

                    for (int i = -valorRef; i <= valorRef; i++) { // linha vizinhanÃ§a
                        for (int j = -valorRef; j <= valorRef; j++) {
                            Color cor = new Color(imgOriginal.getRGB(coluna + j, linha + i));
                            int red = cor.getRed();
                            soma += (double)red * kernel[posicao];
                            posicao++;
                        }
                    }
                    int result = (int)soma;
                    if (result < 0) result = 0;
                    if (result > 255) result = 255;
                    Color novaCor = new Color(result, result, result);
                    imgFiltrada.setRGB(coluna, linha, novaCor.getRGB());
                }
            }
        }

        else {

            for (int linha = valorRef; linha < altura - valorRef; linha++) {
                for (int coluna = valorRef; coluna < largura - valorRef; coluna++) {
                    int soma = 0;
                    int posicao = 0;

                    for (int i = -valorRef; i <= valorRef; i++) { // linha vizinhanÃ§a
                        for (int j = -valorRef; j <= valorRef; j++) {
                            Color cor = new Color(imgOriginal.getRGB(coluna + j, linha + i));
                            int red = cor.getRed();
                            soma += (double)red * (kernel[posicao]/256);
                            posicao++;
                        }
                    }
                    int result = (int)soma;
                    if (result < 0) result = 0;
                    if (result > 255) result = 255;
                    Color novaCor = new Color(result, result, result);
                    imgFiltrada.setRGB(coluna, linha, novaCor.getRGB());
                }
            }
        }

        return imgFiltrada;
    }
    
    
    

}
