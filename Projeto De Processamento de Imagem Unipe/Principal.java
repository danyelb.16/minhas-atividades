import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.imageio.ImageIO;


public class Principal {

	public static void main(String[] args) throws IOException {
		
		Editor Filtros = new Editor();
		File arquivo = new File("C:\\Users\\danye\\Desktop\\Photoshop\\supermercado.jpg");
		File arquivoComRuido = new File("C:\\Users\\danye\\Desktop\\Photoshop\\LENARUIDO.png");
		BufferedImage imagem = ImageIO.read(arquivo);
		BufferedImage imagemComRuido = ImageIO.read(arquivoComRuido);
        double[] horizontal = {-1, -2, -1, 0, 0, 0, 1, 2, 1};
        double[] vertical = {-1, 0, 1, -2, 0, 2, -1, 0, 1};
        double[] linhasOeste = {1, 1, -1, 1, -2, -1, 1, 1, -1};
        double[] laplaciano = {0, -1, 0, -1, 4, -1, 0, -1, 0};
		Scanner entrada = new Scanner(System.in);
		int opo = 0;
				
		
		
		System.out.println("Escolha Uma Das Opções :\n(1)Banda Red\n(2)Banda Green\n(3)Banda Blue\n(4)Para Aumentar A 		  Tonalidade\n"
				+ "(5)Para Escala De Cinza Por Banda\n(6)Para Escala De Cinza Na Banda R\n(7)Para Escala De Cinza Na   		   Banda Negativa\n"
				+ "(8)Para Banda Negativa Em Y\n(9)Binarizao\n(10)Para Brilho Aditivo Em Rgb\n(11)Para Brilho 		 		 Multiplicativo Em Rgb\n"
				+ "(12)Para Brilho Aditivo Em Y\n(13)Para Brilho Multiplicativo Em Y\n(14)Para Aplicar A Média Em 			Kernel 3x3\n"
				+ "(15)Para Tirar Ruido Com Mediana 3x3\n(16)Para Tirar Ruido Utilizando Média E Um Kernel Do Tamanho 		  de Sua Preferencia\n"
				+ "(17)Para Tirar Ruido Utilizando Mediana E Um Kernel Do Tamanho de Sua Preferencia\n"
				+ "(18)Para Utilizar Um Kernel Dentre as Horizontal,Vertical,Linha Oeste e Laplaciano");
		opo = entrada.nextInt();
		
		
		switch(opo) {
			case 1:
		        BufferedImage imagSaidaR = Filtros.bandaR(imagem);
		        File fileR = new File("C:\\\\Users\\\\danye\\\\Desktop\\\\Photoshop\\\\supermercadobandaR.jpg");
		        ImageIO.write(imagSaidaR, "jpg", fileR);
				break;
			case 2:
				 BufferedImage imagSaidaG = Filtros.bandaG(imagem);
				 File fileG = new File("C:\\\\Users\\\\danye\\\\Desktop\\\\Photoshop\\\\supermercadoabandaG.jpg");
				 ImageIO.write(imagSaidaG, "jpg", fileG);
				break;
			case 3:
				BufferedImage imagSaidaB = Filtros.bandaB(imagem);
				File fileB = new File("C:\\\\Users\\\\danye\\\\Desktop\\\\Photoshop\\\\supermercadoB.jpg");
			    ImageIO.write(imagSaidaB, "jpg", fileB);
				break;
			case 4:
				System.out.println("Digite o Valor do Aumento o Diminuio da tonalidade e a opo");
				int op =0;
				int valor = 0;
				valor = entrada.nextInt();
				op = entrada.nextInt();
				BufferedImage imagTonalizada = Filtros.UpTonalidade(imagem, valor,op);
				File fileTonalizado = new File("C:\\\\Users\\\\danye\\\\Desktop\\\\Photoshop\\\\supermercadoTonalizado.					jpg");
			    ImageIO.write(imagTonalizada, "jpg", fileTonalizado);  
				break;
			case 5:
				System.out.println("Digite a Banda que quer escolher como padrao da escala de cinza\n"
						+ "1- Para Red\n2- Para Green\n3- Para Blue");
				
				op = entrada.nextInt();
				BufferedImage imagemEscaladeCinzaBanda = Filtros.EscalaCinzaBanda(imagem,op);
				File fileEscaladeCinzaBanda = new File								     		("C:\\\\Users\\\\danye\\\\Desktop\\\\Photoshop\\\\supermercadoEsacaladeCinzaBanda.jpg");		
			    ImageIO.write(imagemEscaladeCinzaBanda, "jpg", fileEscaladeCinzaBanda);  
				break;
			case 6:
				BufferedImage escaladecinzaBandr = Filtros.EscaladeCinzaBandar(imagem);
				File fileCzBandar = new File("C:\\\\Users\\\\danye\\\\Desktop\\\\Photoshop\\\\supermercadoCinzaBandaR.jpg");
		        ImageIO.write(escaladecinzaBandr, "jpg", fileCzBandar);
				break;	
			case 7:
				BufferedImage escalanegativa = Filtros.BandaNegativa(imagem);
				File fileImagemNegativa = new File("C:\\\\Users\\\\danye\\\\Desktop\\\\Photoshop\\\\supermercadoNegativo.jpg");
		        ImageIO.write(escalanegativa, "jpg", fileImagemNegativa);
				break;
			case 8:
				BufferedImage escalanegativaY = Filtros.BandaNegativaY(imagem);
				File fileImagemNegativaY = new File("C:\\\\Users\\\\danye\\\\Desktop\\\\Photoshop\\\\supermercadoNegativoY.jpg");
		        ImageIO.write(escalanegativaY, "jpg", fileImagemNegativaY);
				break;	
			case 9:
				int limear=100;
				BufferedImage Binarizacao = Filtros.Limiarizacao(imagem,limear);
				File fileBinarizado = new File("C:\\\\Users\\\\danye\\\\Desktop\\\\Photoshop\\\\supermercadoBinarizado.jpg");
		        ImageIO.write(Binarizacao, "jpg", fileBinarizado);
				break;	
			case 10:
				int brilhoadd = 0;
				System.out.println("Digite o valor que deseja aumentar o brilho(Em Rgb)");
				brilhoadd = entrada.nextInt();
				BufferedImage addBrilho = Filtros.BrilhoAddRgb(imagem, brilhoadd);
				File fileBrilhoAddRgb = new File("C:\\\\Users\\\\danye\\\\Desktop\\\\Photoshop\\\\supermercadoBrilhoAddRgb.jpg");
		        ImageIO.write(addBrilho, "jpg", fileBrilhoAddRgb);
				break;		
			case 11:
				double brilhomtt = 0;
				System.out.println("Digite o valor que deseja aumentar o brilho(Em Rgb)");
				brilhomtt = entrada.nextDouble();
				BufferedImage mttBrilho = Filtros.BrilhoMttRgb(imagem, brilhomtt);
				File fileBrilhoMttRgb = new File("C:\\\\Users\\\\danye\\\\Desktop\\\\Photoshop\\\\supermercadoBrilhoMttRgb.jpg");
		        ImageIO.write(mttBrilho, "jpg", fileBrilhoMttRgb);
				break;	
			case 12:
				int brilhoaddy = 0;
				System.out.println("Digite o valor que deseja aumentar o brilho(Em Y)");
				brilhoaddy = entrada.nextInt();
				BufferedImage addBrilhoY = Filtros.BrilhoAddY(imagem, brilhoaddy);
				File fileBrilhoAddY = new File("C:\\\\Users\\\\danye\\\\Desktop\\\\Photoshop\\\\supermercadoBrilhoAddY.jpg");
		        ImageIO.write(addBrilhoY, "jpg", fileBrilhoAddY);
				break;		
			case 13:
				double brilhomtty = 0;
				System.out.println("Digite o valor que deseja aumentar o brilho(Em Y)");
				brilhomtty = entrada.nextDouble();
				BufferedImage mttBrilhoY = Filtros.BrilhoMttY(imagem, brilhomtty);
				File fileBrilhoMttY = new File("C:\\\\Users\\\\danye\\\\Desktop\\\\Photoshop\\\\supermercadoBrilhoMttY.jpg");
		        ImageIO.write(mttBrilhoY, "jpg", fileBrilhoMttY);
				break;	
				
			case 14:
				BufferedImage imagSaidaMedia3x3 = Filtros.media3x3(imagemComRuido);
		        File fileMedia3x3 = new File("C:\\\\Users\\\\danye\\\\Desktop\\\\Photoshop\\\\LenaSemRuidoMedia3x3.png");
		        ImageIO.write(imagSaidaMedia3x3, "png", fileMedia3x3);
				break;
			case 15:
				BufferedImage imagSaidaMediana3x3 = Filtros.mediana3x3(imagemComRuido);
		        File fileMediana3x3 = new File("C:\\\\Users\\\\danye\\\\Desktop\\\\Photoshop\\\\LenaSemRuidoMediana3x3.png");
		        ImageIO.write(imagSaidaMediana3x3, "png", fileMediana3x3);
				break;
			case 16:
				System.out.println("Digite o tamanho da vizinhança desejado:");
				int tamanhoVizinhanca = 0;
				tamanhoVizinhanca = entrada.nextInt();
				BufferedImage imagSaidaMedia = Filtros.mediaTamVizinhanca(imagemComRuido,tamanhoVizinhanca);
		        File fileMediaTamVizinhanca = new File("C:\\\\Users\\\\danye\\\\Desktop\\\\Photoshop\\\\LenaSemRuidoMedia.png");
		        ImageIO.write(imagSaidaMedia, "png", fileMediaTamVizinhanca);
				break;
			case 17:
				System.out.println("Digite o tamanho da vizinhança desejado:");
				tamanhoVizinhanca = entrada.nextInt();
				BufferedImage imagSaidaMediana = Filtros.medianaTamVizinhanca(imagemComRuido,tamanhoVizinhanca);
		        File fileMedianaTamVizinhanca = new File("C:\\\\Users\\\\danye\\\\Desktop\\\\Photoshop\\\\LenaSemRuidoMediana.png");
		        ImageIO.write(imagSaidaMediana, "png", fileMedianaTamVizinhanca);
				break;
			case 18:
				op =0;
				System.out.println("Digite e escolha qual o kernel desejado:\n(1)Para Kernel Horizontal\n(2)Para Kernel Vertical\n(3)Para Kernel Linha Oeste"
						+ "\n(04)Para Kernel de Laplaciano");
				op = entrada.nextInt();
				double [] kernelDesejado = new double[9];
				if(op==1)kernelDesejado = horizontal;
				else if(op==2)kernelDesejado = vertical;
				else if(op==3)kernelDesejado = linhasOeste;
				else if(op==4)kernelDesejado = laplaciano;
				else System.out.println("Opção invalida,reinicie o programa e tente novamente");
				BufferedImage imagSaidaConvolucao = Editor.convolucao(imagemComRuido,kernelDesejado);
		        File fileConvolucao = new File("C:\\\\Users\\\\danye\\\\Desktop\\\\Photoshop\\\\LenaConvolucao.png");
		        ImageIO.write(imagSaidaConvolucao, "png", fileConvolucao);
				break;
			default:
				System.out.println("Opção Invalida");

		}
	}

}
